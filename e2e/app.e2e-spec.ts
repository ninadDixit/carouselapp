import { CarouselAppPage } from './app.po';

describe('carousel-app App', () => {
  let page: CarouselAppPage;

  beforeEach(() => {
    page = new CarouselAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
