export interface CarouselImage {
  albumId: number;
  id: 1;
  title: string;
  url: string;
  thumbnailUrl: string;
}