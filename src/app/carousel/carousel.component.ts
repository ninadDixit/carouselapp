import {Component, OnInit} from '@angular/core';
import {PhotosService} from '../services/photos.service';
import { CarouselImage } from './carousel.image';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {
  photos: CarouselImage[] = [];
  i = 0;
  thumbnail: { url: string; } = { url: '' };

  get canPrev() {
    return (this.i - 1) >= 0;
  }

  get canNext() {
    return (this.i + 1) < this.photos.length;
  }

  getPrev() {
    this.i = this.i === 0 ? 0 : this.i - 1;
    if (this.canPrev) {
      this.displayThumbnail(this.photos[this.i - 1]);
    }
  }

  getNext() {
    this.i = this.i === this.photos.length ? this.i : this.i + 1;
    if (this.canNext) {
      this.displayThumbnail(this.photos[this.i + 1]);
    }
  }

  displayThumbnail(image) {
    this.thumbnail.url = image.thumbnailUrl;
  }

  constructor(private service: PhotosService ) {
  }

  ngOnInit() {
    this.service.getAll().subscribe(photos =>  {
      this.photos = photos;
      this.thumbnail.url = this.photos[0].thumbnailUrl;
    });
  }

}